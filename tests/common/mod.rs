use rand::Rng;

pub const BROKER_SPIN_UP_TIME: u64 = 10;
pub const DEFAULT_BROKER_ADDR: &str = "127.0.0.1:1883";
// pub const DEFAULT_BROKER_ADDR: std::net::SocketAddr = std::net::SocketAddr::new(
//     std::net::IpAddr::V4(std::net::Ipv4Addr::new(127, 0, 0, 1)),
//     1883,
// );

pub fn bit_array_to_byte(a: [bool; 8]) -> u8 {
    let mut b = 0u8;
    for (i, val) in a.iter().rev().enumerate() {
        b |= u8::from(*val) << (i % 8);
    }
    b
}

pub fn make_new_broker() {
    let mut rng = rand::thread_rng();
    let config = librumqttd::Config {
        id: 0,
        router: librumqttd::rumqttlog::Config {
            id: 0,
            dir: std::env::temp_dir(), //std::path::PathBuf::from(r"/tmp/test1"),
            max_segment_size: 10240,
            max_segment_count: 10,
            max_connections: 10001,
        },
        servers: std::collections::HashMap::from([(
            "1".to_string(),
            librumqttd::ServerSettings {
                listen: DEFAULT_BROKER_ADDR
                    .parse()
                    .expect("failed to parse DEFAULT_BROKER_ADDR"),
                cert: None,
                next_connection_delay_ms: 1,
                connections: librumqttd::ConnectionSettings {
                    connection_timeout_ms: 5000,
                    max_client_id_len: 256,
                    throttle_delay_ms: 0,
                    max_payload_size: 5120,
                    max_inflight_count: 200,
                    max_inflight_size: 1024,
                    login_credentials: None,
                },
            },
        )]),
        cluster: None,
        replicator: None,
        // unsure what console is todo learn
        console: librumqttd::ConsoleSettings {
            listen: std::net::SocketAddr::new(
                std::net::IpAddr::V4(std::net::Ipv4Addr::new(127, 0, 0, 1)),
                rng.gen_range(1884..65535),
            ),
        },
    };

    let mut broker = librumqttd::Broker::new(config);
    std::thread::spawn(move || {
        broker.start().unwrap();
    });

    // give broker some time to spin up, depending on computer speed you may want to adjust common::BROKER_SPIN_UP_TIME
    let duration = std::time::Duration::from_millis(BROKER_SPIN_UP_TIME);
    std::thread::sleep(duration);
}
