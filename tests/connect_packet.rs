use std::io::Read;
use std::io::Write;
use std::net::TcpStream;

mod common;

const BROKER_IP_ADDR: Option<&'static str> = option_env!("BROKER_IP_ADDR");

/// tests `[MQTT-3.2.2-1]`
/// testing brokers response to repeated clean session connections
#[test]
fn must_mqtt_3_2_2_1() {
    // connect to existing broker or make an ephemeral one with rumqttd
    let broker_ip_addr = match BROKER_IP_ADDR {
        Some(val) => val,
        None => {
            common::make_new_broker();
            common::DEFAULT_BROKER_ADDR
        }
    };

    let connect_packet = [
        // HEADER fixed http://docs.oasis-open.org/mqtt/mqtt/v3.1.1/os/mqtt-v3.1.1-os.html#_Toc398718029
        // HEADER (fixed): byte 1, packet type + reserved
        common::bit_array_to_byte([false, false, false, true, false, false, false, false]),
        // HEADER (fixed): length
        19u8, // todo create length calculator with algorithm http://docs.oasis-open.org/mqtt/mqtt/v3.1.1/os/mqtt-v3.1.1-os.html#_Toc398718023
        // HEADER variable http://docs.oasis-open.org/mqtt/mqtt/v3.1.1/os/mqtt-v3.1.1-os.html#_Toc398718030
        // HEADER (variable): protocol name
        0u8,
        4u8,
        b'M',
        b'Q',
        b'T',
        b'T',
        // HEADER (variable): protocol level
        4u8,
        // HEADER (variable): Connect flags
        common::bit_array_to_byte([
            false, // user name flag
            false, // password flag
            false, // will retain
            false, false, // will qos
            false, // will flag
            true,  // clean session
            false, // reserved
        ]),
        // HEADER (variable): Keep alive
        0u8,
        30u8,
        // PAYLOAD
        // PAYLOAD: length (maybe just length of client id?
        0u8,
        7u8,
        // PAYLOAD: client ID
        b'T',
        b'e',
        b's',
        b't',
        b'0',
        b'0',
        b'0',
    ];
    // Write bytes corresponding to `&Packet` into the `BytesMut`.

    println!("{:?}", connect_packet);

    let mut stream = TcpStream::connect(broker_ip_addr).unwrap();
    stream.write(&connect_packet[..]).expect("failed write");
    let mut buf = [0; 4];
    stream.read(&mut buf).ok();
    stream
        .shutdown(std::net::Shutdown::Both)
        .expect("tcp shutdown");
    assert_eq!(32u8, buf[0]); // confirm control packet type is CONNACK http://docs.oasis-open.org/mqtt/mqtt/v3.1.1/os/mqtt-v3.1.1-os.html#_Toc398718034
    assert_eq!(2u8, buf[1]); // remaining length should always be 2
    assert_eq!(0u8, buf[2]); // we just started server session present flag should be 0 [MQTT-3.2.2-1]
    assert_eq!(0u8, buf[3]); // assuming all went well we should get a 0 ack flag

    let mut stream = TcpStream::connect(broker_ip_addr).unwrap();
    stream.write(&connect_packet[..]).expect("failed write");
    let mut buf = [0; 4];
    stream.read(&mut buf).ok();
    stream
        .shutdown(std::net::Shutdown::Both)
        .expect("tcp shutdown");
    assert_eq!(32u8, buf[0]); // confirm control packet type is CONNACK http://docs.oasis-open.org/mqtt/mqtt/v3.1.1/os/mqtt-v3.1.1-os.html#_Toc398718034
    assert_eq!(2u8, buf[1]); // remaining length should always be 2
    assert_eq!(0u8, buf[2], "Failed to conform to [MQTT-3.2.2-1]"); // CONNECT had clean session flag set we should get 0 [MQTT-3.2.2-1]
    assert_eq!(0u8, buf[3]); // assuming all went well we should get a 0 ack flag
}

/// tests `[MQTT-3.2.2-2]` and `[MQTT-3.2.2-3]`
/// testing brokers response to repeated non clean session connections
/// This requires broker to be new/restarted, running this test twice on the same broker will fail assuming it's adhering to the spec
#[test]
fn must_mqtt_3_2_2_2_and_mqtt_3_2_2_3() {
    // connect to existing broker or make an ephemeral one with rumqttd
    let broker_ip_addr = match BROKER_IP_ADDR {
        Some(val) => val,
        None => {
            common::make_new_broker();
            common::DEFAULT_BROKER_ADDR
        }
    };

    let connect_packet = [
        // HEADER fixed http://docs.oasis-open.org/mqtt/mqtt/v3.1.1/os/mqtt-v3.1.1-os.html#_Toc398718029
        // HEADER (fixed): byte 1, packet type + reserved
        common::bit_array_to_byte([false, false, false, true, false, false, false, false]),
        // HEADER (fixed): length
        19u8, // todo create length calculator with algorithm http://docs.oasis-open.org/mqtt/mqtt/v3.1.1/os/mqtt-v3.1.1-os.html#_Toc398718023
        // HEADER variable http://docs.oasis-open.org/mqtt/mqtt/v3.1.1/os/mqtt-v3.1.1-os.html#_Toc398718030
        // HEADER (variable): protocol name
        0u8,
        4u8,
        b'M',
        b'Q',
        b'T',
        b'T',
        // HEADER (variable): protocol level
        4u8,
        // HEADER (variable): Connect flags
        common::bit_array_to_byte([
            false, // user name flag
            false, // password flag
            false, // will retain
            false, false, // will qos
            false, // will flag
            false, // clean session
            false, // reserved
        ]),
        // HEADER (variable): Keep alive
        0u8,
        30u8,
        // PAYLOAD
        // PAYLOAD: length (maybe just length of client id?
        0u8,
        7u8,
        // PAYLOAD: client ID
        b'T',
        b'e',
        b's',
        b't',
        b'0',
        b'0',
        b'1',
    ];
    // Write bytes corresponding to `&Packet` into the `BytesMut`.

    println!("{:?}", connect_packet);

    let mut stream = TcpStream::connect(broker_ip_addr).unwrap();
    stream.write(&connect_packet[..]).expect("failed write");
    let mut buf = [0; 4];
    stream.read(&mut buf).ok();
    stream
        .shutdown(std::net::Shutdown::Both)
        .expect("tcp shutdown");
    assert_eq!(32u8, buf[0]); // confirm control packet type is CONNACK http://docs.oasis-open.org/mqtt/mqtt/v3.1.1/os/mqtt-v3.1.1-os.html#_Toc398718034
    assert_eq!(2u8, buf[1]); // remaining length should always be 2
    assert_eq!(
        0u8, buf[2],
        "Failed to conform to  [MQTT-3.2.2-2], this can also happen if broker is not new"
    ); // we just started server session present flag should be 0 [MQTT-3.2.2-2]
    assert_eq!(0u8, buf[3]); // assuming all went well we should get a 0 ack flag

    let mut stream = TcpStream::connect(broker_ip_addr).unwrap();
    stream.write(&connect_packet[..]).expect("failed write");
    let mut buf = [0; 4];
    stream.read(&mut buf).ok();
    stream
        .shutdown(std::net::Shutdown::Both)
        .expect("tcp shutdown");
    assert_eq!(32u8, buf[0]); // confirm control packet type is CONNACK http://docs.oasis-open.org/mqtt/mqtt/v3.1.1/os/mqtt-v3.1.1-os.html#_Toc398718034
    assert_eq!(2u8, buf[1]); // remaining length should always be 2
    assert_eq!(1u8, buf[2], "Failed to conform to [MQTT-3.2.2-3]"); // the server saw us connect with clean-session false, that means it should hold state for us and to ACK that it has done this return 1 here [MQTT-3.2.2-3]
    assert_eq!(0u8, buf[3]); // assuming all went well we should get a 0 ack flag
}

/// tests `[MQTT-3.1.2-2]` and `[MQTT-3.2.2-4]`
/// Servers "Should" but are not required to send error codes for unsupported protocol levels
/// "If a well formed CONNECT Packet is received by the Server, but the Server is unable to process it for some reason, then the Server SHOULD attempt to send a CONNACK packet containing the appropriate non-zero Connect return code from this table"
#[test]
fn should_mqtt_3_1_2_2_and_mqtt_3_2_2_4() {
    // connect to existing broker or make an ephemeral one with rumqttd
    let broker_ip_addr = match BROKER_IP_ADDR {
        Some(val) => val,
        None => {
            common::make_new_broker();
            common::DEFAULT_BROKER_ADDR
        }
    };

    let connect_packet = [
        // HEADER fixed http://docs.oasis-open.org/mqtt/mqtt/v3.1.1/os/mqtt-v3.1.1-os.html#_Toc398718029
        // HEADER (fixed): byte 1, packet type + reserved
        common::bit_array_to_byte([false, false, false, true, false, false, false, false]),
        // HEADER (fixed): length
        19u8, // todo create length calculator with algorithm http://docs.oasis-open.org/mqtt/mqtt/v3.1.1/os/mqtt-v3.1.1-os.html#_Toc398718023
        // HEADER variable http://docs.oasis-open.org/mqtt/mqtt/v3.1.1/os/mqtt-v3.1.1-os.html#_Toc398718030
        // HEADER (variable): protocol name
        0u8,
        4u8,
        b'M',
        b'Q',
        b'T',
        b'T',
        // HEADER (variable): protocol level
        30u8, // use a level significantly higher to future proof test
        // HEADER (variable): Connect flags
        common::bit_array_to_byte([
            false, // user name flag
            false, // password flag
            false, // will retain
            false, false, // will qos
            false, // will flag
            false, // clean session
            false, // reserved
        ]),
        // HEADER (variable): Keep alive
        0u8,
        30u8,
        // PAYLOAD
        // PAYLOAD: length (maybe just length of client id?
        0u8,
        7u8,
        // PAYLOAD: client ID
        b'T',
        b'e',
        b's',
        b't',
        b'0',
        b'0',
        b'2',
    ];
    // Write bytes corresponding to `&Packet` into the `BytesMut`.

    println!("{:?}", connect_packet);

    let mut stream = TcpStream::connect(broker_ip_addr).unwrap();
    stream.write(&connect_packet[..]).expect("failed write");
    let mut buf = [0; 4];
    assert!(stream.read_exact(&mut buf).is_ok(), "Failed to conform to [MQTT-3.1.2-2], connection was closed");
    stream
        .shutdown(std::net::Shutdown::Both)
        .expect("tcp shutdown");
    assert_eq!(32u8, buf[0], "Failed to conform to [MQTT-3.1.2-2], incorrect packet type in CONNACK"); // confirm control packet type is CONNACK http://docs.oasis-open.org/mqtt/mqtt/v3.1.1/os/mqtt-v3.1.1-os.html#_Toc398718034
    assert_eq!(2u8, buf[1]); // remaining length should always be 2
    assert_eq!(0u8, buf[2], "Failed to conform to [MQTT-3.2.2-4], incorrect session present"); // [MQTT-3.2.2-4]
    assert_eq!(1u8, buf[3], "Failed to conform to [MQTT-3.1.2-2], incorrect return code"); // if conforming to [MQTT-3.1.2-2] we should get "0x01 Connection Refused, unacceptable protocol version"
}

/// tests `[MQTT-3.1.0-2]`
/// Broker must disconnect if receiving 2 CONNECT packets
#[test]
fn must_mqtt_3_1_0_2() {
    // connect to existing broker or make an ephemeral one with rumqttd
    let broker_ip_addr = match BROKER_IP_ADDR {
        Some(val) => val,
        None => {
            common::make_new_broker();
            common::DEFAULT_BROKER_ADDR
        }
    };

    let connect_packet = [
        // HEADER fixed http://docs.oasis-open.org/mqtt/mqtt/v3.1.1/os/mqtt-v3.1.1-os.html#_Toc398718029
        // HEADER (fixed): byte 1, packet type + reserved
        common::bit_array_to_byte([false, false, false, true, false, false, false, false]),
        // HEADER (fixed): length
        19u8, // todo create length calculator with algorithm http://docs.oasis-open.org/mqtt/mqtt/v3.1.1/os/mqtt-v3.1.1-os.html#_Toc398718023
        // HEADER variable http://docs.oasis-open.org/mqtt/mqtt/v3.1.1/os/mqtt-v3.1.1-os.html#_Toc398718030
        // HEADER (variable): protocol name
        0u8,
        4u8,
        b'M',
        b'Q',
        b'T',
        b'T',
        // HEADER (variable): protocol level
        4u8,
        // HEADER (variable): Connect flags
        common::bit_array_to_byte([
            false, // user name flag
            false, // password flag
            false, // will retain
            false, false, // will qos
            false, // will flag
            true,  // clean session
            false, // reserved
        ]),
        // HEADER (variable): Keep alive
        0u8,
        30u8,
        // PAYLOAD
        // PAYLOAD: length (maybe just length of client id?
        0u8,
        7u8,
        // PAYLOAD: client ID
        b'T',
        b'e',
        b's',
        b't',
        b'0',
        b'0',
        b'3',
    ];
    // Write bytes corresponding to `&Packet` into the `BytesMut`.

    println!("{:?}", connect_packet);

    let mut stream = TcpStream::connect(broker_ip_addr).unwrap();
    stream.write(&connect_packet[..]).expect("failed write");
    let mut buf = [0; 4];
    assert!(stream.read_exact(&mut buf).is_ok(), "Failed to conform to [MQTT-3.1.2-2], connection was closed");
    assert_eq!(32u8, buf[0]); // confirm control packet type is CONNACK http://docs.oasis-open.org/mqtt/mqtt/v3.1.1/os/mqtt-v3.1.1-os.html#_Toc398718034
    assert_eq!(2u8, buf[1]); // remaining length should always be 2
    assert_eq!(0u8, buf[2], "Failed to conform to [MQTT-3.2.2-1]"); // CONNECT had clean session flag set we should get 0 [MQTT-3.2.2-1]
    assert_eq!(0u8, buf[3]); // assuming all went well we should get a 0 ack flag
    stream.write(&connect_packet[..]).expect("failed write");
    assert!(stream.read_exact(&mut buf).is_err(), "Failed to conform to [MQTT-3.1.0-2], connection left open");
}

/// tests `[MQTT-3.1.2-3]`
/// The Server MUST validate that the reserved flag in the CONNECT Control Packet is set to zero and disconnect the Client if it is not zero
#[test]
fn must_mqtt_3_1_2_3() {
    // connect to existing broker or make an ephemeral one with rumqttd
    let broker_ip_addr = match BROKER_IP_ADDR {
        Some(val) => val,
        None => {
            common::make_new_broker();
            common::DEFAULT_BROKER_ADDR
        }
    };

    let connect_packet = [
        // HEADER fixed http://docs.oasis-open.org/mqtt/mqtt/v3.1.1/os/mqtt-v3.1.1-os.html#_Toc398718029
        // HEADER (fixed): byte 1, packet type + reserved
        common::bit_array_to_byte([false, false, false, true, false, false, false, false]),
        // HEADER (fixed): length
        19u8, // todo create length calculator with algorithm http://docs.oasis-open.org/mqtt/mqtt/v3.1.1/os/mqtt-v3.1.1-os.html#_Toc398718023
        // HEADER variable http://docs.oasis-open.org/mqtt/mqtt/v3.1.1/os/mqtt-v3.1.1-os.html#_Toc398718030
        // HEADER (variable): protocol name
        0u8,
        4u8,
        b'M',
        b'Q',
        b'T',
        b'T',
        // HEADER (variable): protocol level
        4u8,
        // HEADER (variable): Connect flags
        common::bit_array_to_byte([
            false, // user name flag
            false, // password flag
            false, // will retain
            false, false, // will qos
            false, // will flag
            true,  // clean session
            true, // reserved
        ]),
        // HEADER (variable): Keep alive
        0u8,
        30u8,
        // PAYLOAD
        // PAYLOAD: length (maybe just length of client id?
        0u8,
        7u8,
        // PAYLOAD: client ID
        b'T',
        b'e',
        b's',
        b't',
        b'0',
        b'0',
        b'4',
    ];

    let mut stream = TcpStream::connect(broker_ip_addr).unwrap();
    stream.write(&connect_packet[..]).expect("failed write");
    println!("sent packet: {:?}", connect_packet);
    let mut buf = [0; 4];
    assert!(stream.read_exact(&mut buf).is_err(), "Failed to conform to [MQTT-3.1.2-3], connection left open");
}