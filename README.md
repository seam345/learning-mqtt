Attempting to learn MQTT by making a test suite by default it brings up a rumqttd but if you specify A ip:port it can
run against an external broker
e.g. bring up broker `docker run -p 1883:1883 -p 9001:9001 -it  eclipse-mosquitto:2.0 mosquitto -c /mosquitto-no-auth.conf`
then test with`BROKER_IP_ADDR="127.0.0.1:1883" cargo test -- --test-threads=1`

Running with thread=1 to help increase chance of success, as I plan to make a lot of test it reduces the chance of
strange collisions however atm I am getting away without --test-threads=1.

Also, all tests are prefixed with should/must to allow quick filtering of tests that if failed fail the spec `cargo test must -- --test-threads=1`
and test that if failed are only failing a "recommendation" by the spec `cargo test should -- --test-threads=1`


At some point I plan to display the results in a nice webpage using gitlab pages